import { createRouter, createWebHistory } from "vue-router";
import HelloWorld from "./components/HelloWorld.vue";

const routes = [
  { path: "/", component: HelloWorld },
  { path: "/about", component: HelloWorld },
];

const history = createWebHistory();

const router = createRouter({
  history,
  routes,
});

export default router;