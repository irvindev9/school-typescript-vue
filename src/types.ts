export interface EventPerson {
  name: string;
  lastname: string;
  age: number;
}